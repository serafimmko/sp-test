const express = require('express');
const path = require('path');

const app = express();

const APP_PATH = `${__dirname}/dist/sp-test`;

app.use(express.static(APP_PATH));

app.get('/*', function (req, res) {
  res.sendFile(path.join(`${APP_PATH}/index.html`));
});

app.listen(process.env.PORT || 8080);
