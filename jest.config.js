module.exports = {
    preset: 'jest-preset-angular',
    setupFiles: ["jest-canvas-mock", "core-js"],
    setupFilesAfterEnv: ['jest-preset-angular/setupJest'],
    globals: {
        'ts-jest': {
            tsConfig: '<rootDir>/src/tsconfig.spec.json',
            stringifyContentPathRegex: '\\.html$',
            astTransformers: ['jest-preset-angular/InlineHtmlStripStylesTransformer'],
        }
    },
    transform: {
        '^.+\\.(ts|js|html)$': 'ts-jest',
    },
    coverageReporters: ['lcov'],
    coveragePathIgnorePatterns: [
        "<rootDir>/src/app/models/",
        "<rootDir>/src/environments/",
    ]
};
