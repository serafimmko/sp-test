import { Component } from '@angular/core';

@Component({
  selector: 'app-sp-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {

}
