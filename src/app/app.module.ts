import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Routing } from './app-routing.module';
import { AppComponent } from './app.component';
import { MockBackendInterceptor } from './mock-backend/mock-backend.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { SaveButtonComponent } from './components/save-button/save-button.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { IndexResolver } from './containers/index/index.resolver';
import { IndexComponent } from './containers/index/index.component';
import { ShopsListComponent } from './components/shops-list/shops-list.component';
import { ShopItemComponent } from './components/shop-item/shop-item.component';
import { svgIconProviders } from './custom-svg-provider';
import { CustomIconRegistry } from './custom-svg-registry';
import { WorkerBarComponent } from './components/worker-bar/worker-bar.component';
import { WorkerItemComponent } from './components/worker-item/worker-item.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { WarningIconComponent } from './components/warning-icon.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatBadgeModule } from '@angular/material/badge';
import { PipesModule } from './pipes/pipes.module';

@NgModule({
  declarations: [
    AppComponent,
    SaveButtonComponent,
    IndexComponent,
    ShopsListComponent,
    ShopItemComponent,
    WorkerBarComponent,
    WorkerItemComponent,
    WarningIconComponent,
  ],
  imports: [
    BrowserModule,
    Routing,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    PerfectScrollbarModule,
    MatSnackBarModule,
    MatBadgeModule,
    PipesModule,
  ],
  providers: [
    IndexResolver,
    { provide: MatIconRegistry, useClass: CustomIconRegistry },
    svgIconProviders,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MockBackendInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
