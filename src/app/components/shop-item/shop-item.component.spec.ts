import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopItemComponent } from './shop-item.component';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { CustomIconRegistry } from '../../custom-svg-registry';
import { svgIconProviders } from '../../custom-svg-provider';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ShopItemComponent', () => {
  let component: ShopItemComponent;
  let fixture: ComponentFixture<ShopItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatButtonModule,
        HttpClientTestingModule,
      ],
      declarations: [
        ShopItemComponent
      ],
      providers: [
        { provide: MatIconRegistry, useClass: CustomIconRegistry },
        svgIconProviders,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopItemComponent);
    component = fixture.componentInstance;
    component.shop = {
      full_address: '',
      name: '',
      id: 1,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
