import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Shop } from '../../app';

@Component({
  selector: 'app-sp-shop-item',
  templateUrl: './shop-item.component.html',
  styleUrls: ['./shop-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShopItemComponent {
  @Input() shop: Shop;
  @Input() disabled = false;

}
