import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Shop, Worker } from '../../app';

@Component({
  selector: 'app-sp-worker-item',
  templateUrl: './worker-item.component.html',
  styleUrls: ['./worker-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WorkerItemComponent {
  @Input() worker: Worker | null;
  @Output() removeShop = new EventEmitter<Shop>();
  @Output() removeWorker = new EventEmitter<void>();

  trackShopFn(idx: number, shop: Shop): string {
    return `${ idx }__${ shop.name }`;
  }
}
