import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerItemComponent } from './worker-item.component';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CustomIconRegistry } from '../../custom-svg-registry';
import { svgIconProviders } from '../../custom-svg-provider';
import { Shop } from '../../app';
import { ShopItemComponent } from '../shop-item/shop-item.component';
import { WarningIconComponent } from '../warning-icon.component';
import { PipesModule } from '../../pipes/pipes.module';

describe('WorkerItemComponent', () => {
  let component: WorkerItemComponent;
  let fixture: ComponentFixture<WorkerItemComponent>;
  const mockShop: Shop = {
    id: 11,
    full_address: 'test_add',
    name: 'test',
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatButtonModule,
        HttpClientTestingModule,
        PipesModule,
      ],
      declarations: [
        WorkerItemComponent,
        ShopItemComponent,
        WarningIconComponent,
      ],
      providers: [
        { provide: MatIconRegistry, useClass: CustomIconRegistry },
        svgIconProviders,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle trackBy', () => {
    expect(component.trackShopFn(12, mockShop)).toEqual('12__test');
  });
});
