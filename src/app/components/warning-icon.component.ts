import { Component } from '@angular/core';

@Component({
  selector: 'app-sp-warning-icon',
  template: `
    <div class="icon">
      <mat-icon svgIcon="add-warning"></mat-icon>
    </div>
  `,
  styles: [`
    .icon {
      display: flex;
      align-items: center;
      justify-content: center;
      width: 24px;
      height: 24px;
      margin-left: 8px;
      background-color: #fff4e6;
      border-radius: 50%;
    }

    mat-icon {
      width: 16px;
      height: 16px;
      font-size: 16px;
    }
  `]
})
export class WarningIconComponent {
}
