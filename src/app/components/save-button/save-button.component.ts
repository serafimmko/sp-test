import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-sp-save-button',
  templateUrl: './save-button.component.html',
  styleUrls: ['./save-button.component.scss']
})
export class SaveButtonComponent {
  @Input() saveAllowed: boolean;
  @Output() save = new EventEmitter<void>();
}
