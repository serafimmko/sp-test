import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopsListComponent } from './shops-list.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ShopItemComponent } from '../shop-item/shop-item.component';
import { WarningIconComponent } from '../warning-icon.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { OrderByPipe } from '../../pipes/order-by.pipe';
import { Shop } from '../../app';

describe('ShopsListComponent', () => {
  let component: ShopsListComponent;
  let fixture: ComponentFixture<ShopsListComponent>;
  const mockShop: Shop = {
    id: 11,
    name: 'test',
    full_address: 'test_add',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatButtonModule,
        HttpClientTestingModule,
        PerfectScrollbarModule,
      ],
      declarations: [
        ShopsListComponent,
        ShopItemComponent,
        WarningIconComponent,
        OrderByPipe,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle trackBy', () => {
    expect(component.trackByShopFn(12, mockShop)).toEqual('12__test');
  });

  it('should handle click', () => {
    spyOn(component.selectShop, 'emit');
    component.disabled = true;
    component.handleClick(mockShop);
    expect(component.selectShop.emit).not.toBeCalled();

    component.disabled = false;
    component.handleClick(mockShop);
    expect(component.selectShop.emit).toBeCalledWith(mockShop);
  });
});
