import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Shop } from '../../app';

@Component({
  selector: 'app-sp-shops-list',
  templateUrl: './shops-list.component.html',
  styleUrls: ['./shops-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShopsListComponent {
  @Input() shops: Shop[] = [];
  @Output() selectShop = new EventEmitter<Shop>();
  @Input() disabled: boolean;

  trackByShopFn(idx: number, item: Shop): string {
    return `${ idx }__${ item.name }`;
  }

  handleClick(shop: Shop) {
    if (!this.disabled) {
      this.selectShop.emit(shop);
    }
  }
}
