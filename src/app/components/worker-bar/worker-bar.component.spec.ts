import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerBarComponent } from './worker-bar.component';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CustomIconRegistry } from '../../custom-svg-registry';
import { svgIconProviders } from '../../custom-svg-provider';
import { Worker } from '../../app';
import { MatBadgeModule } from '@angular/material/badge';

describe('WorkerBarComponent', () => {
  let component: WorkerBarComponent;
  let fixture: ComponentFixture<WorkerBarComponent>;
  const mockWorker: Worker = {
    shops: [],
    id: 11,
    full_name: 'test_name',
    logo: '',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatButtonModule,
        HttpClientTestingModule,
        MatBadgeModule,
      ],
      declarations: [
        WorkerBarComponent,
      ],
      providers: [
        { provide: MatIconRegistry, useClass: CustomIconRegistry },
        svgIconProviders,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle trackBy', () => {
    expect(component.trackWorkerFn(12, mockWorker)).toEqual('12__test_name');
  });
});
