import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Worker } from '../../app';

@Component({
  selector: 'app-sp-worker-bar',
  templateUrl: './worker-bar.component.html',
  styleUrls: ['./worker-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WorkerBarComponent {
  @Input() selected: Worker;
  @Input() workers: Worker[] = [];
  @Output() add = new EventEmitter<void>();
  @Output() selectWorker = new EventEmitter<Worker>();

  readonly MAX_WORKERS_COUNT = 6;

  trackWorkerFn(idx: number, item: Worker): string {
    return `${ idx }__${ item.full_name }`;
  }
}

