import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Shop, CreateWorkerShopRequest, WorkerResponse } from '../app';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getWorkers(): Observable<WorkerResponse[]> {
    return this.httpClient.get<WorkerResponse[]>('/workers');
  }

  getShops(): Observable<Shop[]> {
    return this.httpClient.get<Shop[]>('/shops');
  }

  saveWorkers(workerData: CreateWorkerShopRequest[]): Observable<null> {
    return this.httpClient.post<null>('/save', { workerData });
  }
}

