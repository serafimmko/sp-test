import { async, TestBed } from '@angular/core/testing';

import { CreateWorkerShopRequest, Shop, Worker } from '../app';
import { HttpClientTestingModule, HttpTestingController, RequestMatch } from '@angular/common/http/testing';
import { AppService } from './app.service';
import { MatIconRegistry } from '@angular/material/icon';
import { CustomIconRegistry } from '../custom-svg-registry';
import { svgIconProviders } from '../custom-svg-provider';

describe('MockBackedService', () => {
  let service: AppService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        { provide: MatIconRegistry, useClass: CustomIconRegistry },
        svgIconProviders,
      ],
    });
    service = TestBed.inject(AppService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return mock workers', async(() => {
    service.getWorkers().subscribe((workers: Worker[]) => {
      expect(workers).toEqual([]);
    });

    httpMock.match(({ url, method }: RequestMatch) => {
      return url === '/workers' && method === 'GET';
    })[0].flush([]);
  }));

  it('should return mock shops', async(() => {
    service.getShops().subscribe((shops: Shop[]) => {
      expect(shops).toEqual([]);
    });

    httpMock.match(({ url, method }: RequestMatch) => {
      return url === '/shops' && method === 'GET';
    })[0].flush([]);
  }));

  it('should return save result', async(() => {
    const mockData: CreateWorkerShopRequest[] = [{
      12: [4, 6],
    }];

    service.saveWorkers(mockData).subscribe((result: any) => {
      expect(result).toEqual(null);
    });

    httpMock.match(({ url, method }: RequestMatch) => {
      return url === '/save' && method === 'POST';
    })[0].flush(null);
  }));
});
