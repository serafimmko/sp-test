export interface Shop {
  id: number;
  name: string;
  full_address: string;
}

export interface WorkerResponse {
  id: number;
  full_name: string;
  logo: string;
  shops: Shop[];
}

export interface Worker  extends WorkerResponse {
  shops: Shop[];
}

export interface CreateWorkerShopRequest {
  [key: number]: number[];
}
