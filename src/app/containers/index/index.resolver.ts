import { combineLatest, Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { AppService } from '../../services/app.service';
import { map } from 'rxjs/operators';

@Injectable()
export class IndexResolver implements Resolve<Observable<any>> {
  constructor(
    private appService: AppService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const workers$ = this.appService.getWorkers();
    const shops$ = this.appService.getShops();

    return combineLatest([workers$, shops$])
      .pipe(
        map(([workers, shops]) => ({ workers, shops })),
      );
  }
}
