import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexComponent } from './index.component';
import { SaveButtonComponent } from '../../components/save-button/save-button.component';
import { ActivatedRoute } from '@angular/router';
import { instance, mock, when } from 'ts-mockito';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { of } from 'rxjs';
import { ShopsListComponent } from '../../components/shops-list/shops-list.component';
import { WorkerItemComponent } from '../../components/worker-item/worker-item.component';
import { WorkerBarComponent } from '../../components/worker-bar/worker-bar.component';
import { ShopItemComponent } from '../../components/shop-item/shop-item.component';
import { MatButtonModule } from '@angular/material/button';
import { svgIconProviders } from '../../custom-svg-provider';
import { CustomIconRegistry } from '../../custom-svg-registry';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { WarningIconComponent } from '../../components/warning-icon.component';
import { PipesModule } from '../../pipes/pipes.module';
import { MatBadgeModule } from '@angular/material/badge';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Shop, Worker } from '../../app';

describe('IndexComponent', () => {
  let component: IndexComponent;
  let fixture: ComponentFixture<IndexComponent>;
  let testWorker: Worker;
  let testShop: Shop;
  let secondTestShop: Shop;
  const mockActivatedRoute = mock<ActivatedRoute>(ActivatedRoute);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatIconModule,
        MatButtonModule,
        HttpClientTestingModule,
        PerfectScrollbarModule,
        PipesModule,
        MatBadgeModule,
        MatSnackBarModule,
      ],
      declarations: [
        IndexComponent,
        SaveButtonComponent,
        ShopsListComponent,
        ShopItemComponent,
        WorkerItemComponent,
        WorkerBarComponent,
        SaveButtonComponent,
        WarningIconComponent,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useFactory: () => instance(mockActivatedRoute),
        },
        { provide: MatIconRegistry, useClass: CustomIconRegistry },
        svgIconProviders,
      ]
    });
  }));

  beforeEach(() => {
    testWorker = {
      id: 13,
      full_name: 'test',
      logo: '',
      shops: [],
    };
    testShop = {
      id: 1,
      full_address: 'test_addr',
      name: 'test',
    };
    secondTestShop = {
      id: 2,
      full_address: 'test_addr',
      name: 'test2',
    };

    when(mockActivatedRoute.data)
      .thenReturn(of({
        result: { shops: [testShop], workers: [testWorker] },
      }));
    fixture = TestBed.createComponent(IndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.shops).toEqual([testShop]);
    expect(component.workers).toEqual([{ ...testWorker, shops: [] }]);
  });

  it('should handle add new worker', () => {
    component.workers = [testWorker];
    component.handleAdd();
    expect(component.selectedWorkers).toEqual([testWorker]);
    expect(component.workers).toEqual([]);
    expect(component.selectedWorker).toEqual(testWorker);
  });

  it('should handle selectWorker', () => {
    component.handleSelectWorker(testWorker);
    expect(component.selectedWorker).toEqual(testWorker);
  });

  it('should handle selectShop', () => {
    component.selectedWorker = testWorker;
    component.selectedWorkers = [testWorker];
    component.shops = [testShop];
    component.handleSelectShop(testShop);

    expect(component.selectedWorker.shops).toEqual([testShop]);
    expect(component.selectedWorkers).toEqual([testWorker]);
    expect(component.shops).toEqual([]);
  });

  it('should handle removeShop', () => {
    component.selectedWorker = {
      id: 13,
      full_name: 'test',
      logo: '',
      shops: [],
    };
    component.selectedWorkers = [{
      id: 13,
      full_name: 'test',
      logo: '',
      shops: [secondTestShop],
    }];
    component.handleRemoveShop(secondTestShop);

    expect(component.selectedWorker.shops).toEqual([]);
    expect(component.selectedWorkers).toEqual([{ ...testWorker }]);
    expect(component.shops).toEqual([testShop, secondTestShop]);
  });

  it('should handle removeWorker', () => {
    const secondTestWorker: Worker = {
      id: 14,
      full_name: 'test',
      logo: '',
      shops: [{ ...secondTestShop }],
    };
    component.selectedWorker = secondTestWorker;

    component.handleRemoveWorker();
    expect(component.shops).toEqual([testShop, secondTestShop]);
    expect(component.selectedWorkers).toEqual([]);
    expect(component.selectedWorker).toBeNull();
    expect(component.workers).toEqual([testWorker, { ...secondTestWorker, shops: [] }]);
  });
});
