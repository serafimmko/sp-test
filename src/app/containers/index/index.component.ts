import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CreateWorkerShopRequest, Shop, Worker, WorkerResponse } from '../../app';
import { pluck, takeUntil } from 'rxjs/operators';
import { AutoUnsubscribe, AutoUnsubscribeComponent } from '../../decorators/auto-unsubscribe.decorator';
import { AppService } from '../../services/app.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@AutoUnsubscribe()
@Component({
  selector: 'app-sp-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit, AutoUnsubscribeComponent {
  workers: WorkerResponse[] = [];
  selectedWorkers: Worker[] = [];
  shops: Shop[] = [];
  selectedWorker: Worker | null;

  componentDestroy: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private appService: AppService,
    private cdr: ChangeDetectorRef,
    private snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data
      .pipe(
        pluck('result'),
        takeUntil(this.componentDestroy()),
      )
      .subscribe(({ workers, shops }: { workers: WorkerResponse[], shops: Shop[] }) => {
        this.shops = shops;
        this.workers = workers.map((worker: WorkerResponse) => ({ ...worker, shops: [] }));
      });

  }

  handleSave() {
    const data: CreateWorkerShopRequest[] = this.selectedWorkers.map((worker: Worker) => ({
      [worker.id]: worker.shops.map(({ id }) => id).sort((a: number, b: number) => a - b),
    }));
    this.appService.saveWorkers(data)
      .pipe(
        takeUntil(this.componentDestroy())
      )
      .subscribe(() => {
        this.snackBar.open('Успешно сохранено', '', {
          duration: 1000,
          horizontalPosition: 'start',
        });
      });
  }

  handleAdd() {
    const newWorker = this.workers.pop();
    this.selectedWorkers = this.selectedWorkers.concat(newWorker);
    this.selectedWorker = newWorker;
  }

  handleSelectWorker($event: Worker) {
    this.selectedWorker = $event;
  }

  handleSelectShop($event: Shop) {
    this.selectedWorker = {
      ...this.selectedWorker,
      shops: this.selectedWorker.shops = this.selectedWorker.shops.concat($event),
    };
    this.selectedWorkers = this.updateSelectedWorkers();

    this.shops = this.shops.filter(({ id }) => id !== $event.id);
  }

  handleRemoveShop($event: Shop): void {
    this.selectedWorker = {
      ...this.selectedWorker,
      shops: this.selectedWorker.shops.filter(({ id }) => id !== $event.id),
    };
    this.selectedWorkers = this.updateSelectedWorkers();

    this.shops = this.shops.concat($event);
  }

  handleRemoveWorker(): void {
    const removingWorkerId = this.selectedWorker.id;
    const shops = this.selectedWorker.shops;
    shops.forEach((shop: Shop) => this.handleRemoveShop(shop));
    this.workers = this.workers.concat(this.selectedWorker);
    this.selectedWorker = this.getNextWorker();
    this.selectedWorkers = this.selectedWorkers.filter(({ id }) => id !== removingWorkerId);
  }

  private updateSelectedWorkers(): Worker[] {
    return this.selectedWorkers.map((worker: Worker) => {
      if (worker.id === this.selectedWorker.id) {
        worker.shops = this.selectedWorker.shops;
      }
      return worker;
    });
  }

  private getNextWorker(): Worker | null {
    const selectedWorkerIdx = this.selectedWorkers.findIndex(({ id }) => id === this.selectedWorker.id);
    const previousWorker = this.selectedWorkers.find((worker: Worker, idx: number) => idx - selectedWorkerIdx === -1);
    const nextWorker = this.selectedWorkers.find((worker: Worker, idx: number) => idx - selectedWorkerIdx === 1);
    return previousWorker || nextWorker || null;
  }
}
