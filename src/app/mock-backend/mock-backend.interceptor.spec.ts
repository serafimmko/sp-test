import { TestBed } from '@angular/core/testing';

import { MockBackendInterceptor } from './mock-backend.interceptor';

describe('MockBackendInterceptor', () => {
  let interceptor: MockBackendInterceptor;

  beforeEach(() => {
      TestBed.configureTestingModule({
        providers: [
          MockBackendInterceptor,
        ]
      });
      interceptor = TestBed.inject(MockBackendInterceptor);
    }
  );

  it('should be created', () => {
    expect(interceptor).toBeTruthy();
  });

});
