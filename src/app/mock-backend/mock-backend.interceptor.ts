import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
// @ts-ignore
import * as workersResponse from './mock-workers.json';
// @ts-ignore
import * as shopsResponse from './mock-shops.json';
import { Shop, WorkerResponse } from '../app';

@Injectable()
export class MockBackendInterceptor implements HttpInterceptor {
  // tslint:disable-next-line:no-string-literal
  workers: WorkerResponse[] = (workersResponse as any)['default'];
  // tslint:disable-next-line:no-string-literal
  shops: Shop[] = (shopsResponse as any)['default'];

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { method, url } = request;
    if (method === 'GET') {
      if (url === '/workers') {
        return of(new HttpResponse({ status: 200, body: this.workers }));
      } else if (url === '/shops') {
        return of(new HttpResponse({ status: 200, body: this.shops }));
      }
    } else if (method === 'POST') {
      if (url === '/save') {
        return of(new HttpResponse({ status: 201, body: null }));
      }
    }
    next.handle(request);
  }
}
