import { RouterModule, Routes } from '@angular/router';
import { IndexResolver } from './containers/index/index.resolver';
import { IndexComponent } from './containers/index/index.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'index',
  },
  {
    path: 'index',
    pathMatch: 'full',
    component: IndexComponent,
    resolve: {
      result: IndexResolver,
    }
  },
];

export const Routing = RouterModule.forRoot(routes, {
  initialNavigation: 'enabled',
  onSameUrlNavigation: 'reload',
  enableTracing: false,
});
