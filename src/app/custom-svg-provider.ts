// tslint:disable:max-line-length
import { SVG_ICONS } from './custom-svg-registry';

export const svgIconProviders = [
  {
    provide: SVG_ICONS,
    useValue: {
      name: 'add-arrow',
      svgSource:
        '<svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.2656 4.0208l.7188.6875-2.1953 2.289H9v1H2.8086l2.1758 2.2735-.7188.6875-3.332-3.4687 3.332-3.4688zM11.0038 6.9974h-.9989v1h.9989v-1zM12.0001 6.9974v1h1v-1h-1zM15.0001 7.9974v-1h-1v1h1z" fill="#424242"/></svg>'
    },
    multi: true,
  },
  {
    provide: SVG_ICONS,
    useValue: {
      name: 'add-warning',
      svgSource:
        '<svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14.8113 11.2995L8.9467 1.5443C8.7436 1.203 8.3842 1 7.9857 1c-.3984 0-.7577.2031-.9609.5443l-5.8645 9.7552a1.1244 1.1244 0 00-.0157 1.1302c.198.3516.573.5703.9766.5703h11.7292c.4036 0 .7786-.2187.9765-.5703a1.1245 1.1245 0 00-.0156-1.1302zM8.6524 11H7.3191V9.6667h1.3333V11zm0-2.6667H7.3191V5h1.3333v3.3333z" fill="#FF9500"/></svg>'
    },
    multi: true,
  },
  {
    provide: SVG_ICONS,
    useValue: {
      name: 'worker-remove',
      svgSource:
        '<svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.496 1C5.6759 1 5 1.6758 5 2.496V3H2v1h1v8.5c0 .8242.6758 1.5 1.5 1.5h6c.8242 0 1.5-.6758 1.5-1.5V4h1V3h-3v-.504C10 1.6759 9.3242 1 8.504 1H6.496zm0 1h2.008c.2812 0 .496.2148.496.496V4h2v8.5c0 .2812-.2188.5-.5.5h-6c-.2813 0-.5-.2188-.5-.5V4h2V2.496C6 2.2149 6.2148 2 6.496 2zM5 5v7h1V5H5zm2 0v7h1V5H7zm2 0v7h1V5H9z" fill="#ADADAD"/></svg>'
    },
    multi: true,
  },
  {
    provide: SVG_ICONS,
    useValue: {
      name: 'add-worker',
      svgSource:
        '<svg width="60" height="48" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
        '  <g clip-path="url(#clip0)">\n' +
        '    <circle cx="24" cy="24" r="18" stroke="#000" stroke-opacity=".1" stroke-width="2"/>\n' +
        '    <circle cx="24" cy="20" r="6" stroke="#000" stroke-opacity=".1" stroke-width="2"/>\n' +
        '    <path d="M12 36c1.7467-4.6608 6.4599-8 12-8s10.2533 3.3392 12 8" stroke="#000" stroke-opacity=".1" stroke-width="2"/>\n' +
        '    <rect x="28" y="-5" width="27" height="27" rx="13.5" fill="#F2F2F2"/>\n' +
        '    <path d="M41.25 2.25v6h-6v1.5h6v6h1.5v-6h6v-1.5h-6v-6h-1.5z" fill="#000" fill-opacity=".1"/>\n' +
        '  </g>\n' +
        '  <defs>\n' +
        '    <clipPath id="clip0">\n' +
        '      <path fill="#fff" d="M0 0h60v48H0z"/>\n' +
        '    </clipPath>\n' +
        '  </defs>\n' +
        '</svg>'
    },
    multi: true,
  },
];
// tslint:enable:max-line-length
