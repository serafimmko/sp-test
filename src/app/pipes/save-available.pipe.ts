import { Pipe, PipeTransform } from '@angular/core';
import { Shop, Worker } from '../app';

@Pipe({
  name: 'saveAvailable'
})
export class SaveAvailablePipe implements PipeTransform {

  transform(workers: Worker[], shopsInput: Shop[]): boolean {
    const isShopsEmpty = shopsInput.length === 0;
    const noEmptyWorkers = workers.every(({ shops }) => shops.length !== 0);
    return (isShopsEmpty && noEmptyWorkers);
  }

}
