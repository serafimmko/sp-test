import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'plural',
})
export class PluralPipe implements PipeTransform {
  protected readonly nonBreakingSpace = `\u00A0`;

  transform(num: number, one: string, two: string, five: string): string {

    let result: string;

    let n = Math.abs(num);

    n %= 100;

    if (n >= 5 && n <= 20) {
      result = `${ num }${ this.nonBreakingSpace }${ five }`;
    } else {
      n %= 10;

      if (n === 1) {
        result = `${ num }${ this.nonBreakingSpace }${ one }`;
      } else if (n >= 2 && n <= 4) {
        result = `${ num }${ this.nonBreakingSpace }${ two }`;
      } else {
        result = `${ num }${ this.nonBreakingSpace }${ five }`;
      }
    }

    return result;

  }

}
