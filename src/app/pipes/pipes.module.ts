import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PluralPipe } from './plural.pipe';
import { OrderByPipe } from './order-by.pipe';
import { SaveAvailablePipe } from './save-available.pipe';

@NgModule({
  declarations: [
    PluralPipe,
    OrderByPipe,
    SaveAvailablePipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PluralPipe,
    OrderByPipe,
    SaveAvailablePipe,
  ]
})
export class PipesModule {
}
