import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {

  transform<T>(items: T[], ...keys: string[]): T[] {
    if (!items || !keys.length) {
      return items;
    }

    return items.sort((a, b) => this.compare(a, b, keys));
  }

  compare(a: any, b: any, keys: string[]): number {
    let valueA;
    let valueB;
    for (const key of keys) {
      valueA = isNaN(+a[key]) ? a[key] : +a[key];
      valueB = isNaN(+b[key]) ? b[key] : +b[key];
      if (valueA !== valueB) {
        break;
      }
    }

    return (valueA < valueB) ? -1 : (valueA > valueB) ? 1 : 0;
  }
}
