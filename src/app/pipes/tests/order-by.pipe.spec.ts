import { OrderByPipe } from '../order-by.pipe';

describe('OrderBy pipe', () => {
    const pipe: OrderByPipe = new OrderByPipe();

    it('should return source empty array', () => {
        const result = pipe.transform([]);
        expect(result).toEqual([]);
    });

    it('should sort array by 1 key', () => {
        const result = pipe.transform([
            { a: 0, b: 1 },
            { a: 1, b: 0 },
            { a: 0, b: 0 },
            { a: 1, b: 1 },
        ], 'a');
        expect(result).toEqual([
            { a: 0, b: 1 },
            { a: 0, b: 0 },
            { a: 1, b: 0 },
            { a: 1, b: 1 },
        ]);
    });

    it('should sort array by 2 keys', () => {
        const result = pipe.transform([
            { a: 0, b: 1 },
            { a: 1, b: 0 },
            { a: 0, b: 0 },
            { a: 1, b: 1 },
        ], 'a', 'b');
        expect(result).toEqual([
            { a: 0, b: 0 },
            { a: 0, b: 1 },
            { a: 1, b: 0 },
            { a: 1, b: 1 },
        ]);
    });

    it('should sort array by string key', () => {
        const result = pipe.transform([
            { a: 'a', b: 'b' },
            { a: 'a', b: 'a' },
            { a: 'b', b: 'b' },
            { a: 'b', b: 'a' },
        ], 'a');
        expect(result).toEqual([
            { a: 'a', b: 'b' },
            { a: 'a', b: 'a' },
            { a: 'b', b: 'b' },
            { a: 'b', b: 'a' },
        ]);
    });

    it('should sort array by 2 string keys', () => {
        const result = pipe.transform([
            { a: 'a', b: 'b' },
            { a: 'a', b: 'a' },
            { a: 'b', b: 'b' },
            { a: 'b', b: 'a' },
        ], 'a', 'b');
        expect(result).toEqual([
            { a: 'a', b: 'a' },
            { a: 'a', b: 'b' },
            { a: 'b', b: 'a' },
            { a: 'b', b: 'b' },
        ]);
    });
});
