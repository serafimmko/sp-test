import { SaveAvailablePipe } from '../save-available.pipe';
import { Shop, Worker } from '../../app';

describe('SaveAvailablePipe', () => {
  let pipe: SaveAvailablePipe;
  const mockWorkers: Worker[] = [{
    logo: '',
    full_name: '',
    id: 12,
    shops: [],
  }];
  const mockShops: Shop[] = [{
    name: 'test',
    full_address: 'test_addr',
    id: 1,
  }];

  beforeEach(() => {
    pipe = new SaveAvailablePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return false on not-empty shops', () => {
    expect(pipe.transform(mockWorkers, mockShops)).toBeFalsy();
  });

  it('should return false on worker without shops', () => {
    expect(pipe.transform(mockWorkers, [])).toBeFalsy();
  });

  it('should return true', () => {
    const validWorker = {
      ...mockWorkers[0],
      shops: mockShops,
    };
    expect(pipe.transform([validWorker], [])).toBeTruthy();
  });
});
