import { PluralPipe } from '../plural.pipe';

describe('Plural pipe', () => {
    const pipe = new PluralPipe();
    const nonBreakingSpace = `\u00A0`;

    const expectFiveTwenty = (item: number) => {
        const val = pipe.transform(item, 'магазин', 'магазина', 'магазинов');
        expect(val).toEqual(`${ item }${ nonBreakingSpace }магазинов`);
    };

    const expectOne = (item: number) => {
        const val = pipe.transform(item, 'магазин', 'магазина', 'магазинов');
        expect(val).toEqual(`${ item }${ nonBreakingSpace }магазин`);
    };

    const expectTwo = (item: number) => {
        const val = pipe.transform(item, 'магазин', 'магазина', 'магазинов');
        expect(val).toEqual(`${ item }${ nonBreakingSpace }магазина`);
    };

    it('should test n>= 5 && n <= 20', () => {
        const expectedNumArr = Array.from({ length: 16 }, (v, k) => k + 5 + 100);
        expectedNumArr.forEach(expectFiveTwenty);
    });

    it('should test last n === 1', () => {
        const expectedNumArr = Array.from({ length: 16 }, (v, k) => (k + 5) * 100 + 1);
        expectedNumArr.forEach(expectOne);
    });

    it('should test last n >= 2 && n <= 4', () => {
        let expectedNumArr = Array.from({ length: 16 }, (v, k) => (k + 5) * 100 + 2);
        expectedNumArr.forEach(expectTwo);

        expectedNumArr = Array.from({ length: 16 }, (v, k) => (k + 5) * 100 + 3);
        expectedNumArr.forEach(expectTwo);

        expectedNumArr = Array.from({ length: 16 }, (v, k) => (k + 5) * 100 + 4);
        expectedNumArr.forEach(expectTwo);
    });

    it('should test default', () => {
        const expectedNumArr = Array.from({ length: 16 }, (k, v) => +`${v + 5}${v + 5}`);
        expectedNumArr.forEach(expectFiveTwenty);
    });

});
